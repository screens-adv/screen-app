# Screen App

## Requirements
- npm (4.x)
- node (v7.x)
- pm2

## Installation
```bash
git clone https://bitbucket.org/screens-adv/screen-app.git
cd screen-app
npm install
# Dev
npm run start-dev
# Prod
pm2 start --name screen-app start.js
```