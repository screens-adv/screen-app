// Libs
var path = require('path');

// Init app module
window.app = angular.module('app', []);

// Setup app directive
app.directive('app', function () {
    return {
        restrict: 'EA',
        templateUrl: 'src/app/app.html',
        controller: 'appController'
    }
});