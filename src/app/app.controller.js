var fs = require('fs');

app.controller('appController',
    [ 'adsManager', 'player', 'viewLogger', 'signal', 'settings', 'hostScreenId', 'hostScreenRequest', 'testNetwork', 'resources',
    function (
    /*codeUpdates, */adsManager, player, viewLogger, signal, settings, hostScreenId, hostScreenRequest, testNetwork, resources
) {

    function startHostScreenSetup () {
        viewLogger.log('Please setup host screen first: Requesting code..');
        // Get request code
        hostScreenRequest.getRequestCode().then(function (code) {
            viewLogger.log('Please setup host screen first (Request code '+code+')');
            var attemptsCounter = 1;
            // Start verification
            hostScreenRequest.startVerifyingRequest(code, function onAttempt() {
                viewLogger.log('Please setup host screen first (Request code '+code+'), Attempt '+attemptsCounter);
                console.log(Math.random())
                attemptsCounter++;
            }, function onVerify (hostScreenId) {
                // Host screen verified
                viewLogger.log('Code verified, reloading..');
                setTimeout(function () {
                    hostScreenRequest.saveHostScreenId(hostScreenId);
                    window.location.reload();
                }, 2000);
            });
        }).catch(function (err) {
            console.warn(err);
            viewLogger.log('Please setup host screen first: Error requesting code');
        });
    }

    function initApp (contentDomain) {

        // Current settings
        console.log('Current settings', settings);

        // Start sending signal
        signal.startSending();

        // Start checking for ads updates
        adsManager.startWatchingForAdsDataUpdates(

            // On ads data are up-to-date
            function onIsUpToDate () {
                adsManager.getAdsData().then(console.log);
                var d = new Date();
                console.log(
                    'Ads data is up-to-date '+d.getHours()+':'+d.getMinutes()
                );
                
                // Ensure ads content is available
                viewLogger.log('Loading screens content..');
                adsManager.ensureScreensAdsContentIsAvailable().then(function () {
                    viewLogger.log('Loading business content..');
                    adsManager.ensureAdsContentIsAvailable(

                        // Today content is ready
                        function onTodayContentReady() {
                            console.log('Today content is ready');
                            player.start(contentDomain);
                        },
                        
                        // Tomorrow content is ready
                        function onTomorrowContentReady() {
                            console.log('Tomorrow content is ready');
                        }

                    )
                }).catch(function (err) {
                    viewLogger.log('Error loading screens content..');
                    console.warn('Error loading screens ads content', err);
                });
            },

            // On ads data update
            function onUpdate () {
                var d = new Date();
                console.info(
                    'Ads data updated '+d.getHours()+':'+d.getMinutes()
                );

                // Remove trash files
                adsManager.removeUnusedContent().then(function () {
                    console.log('Unused files removed');
                });
            
                // Ensure ads content is available
                viewLogger.log('Loading screens content..');
                adsManager.ensureScreensAdsContentIsAvailable().then(function () {
                    viewLogger.log('Loading business content..');
                    adsManager.ensureAdsContentIsAvailable(

                        // Today content is ready
                        function onTodayContentReady() {
                            console.log('Today content is ready');
                            player.start(contentDomain);
                        },
                        
                        // Tomorrow content is ready
                        function onTomorrowContentReady() {
                            console.log('Tomorrow content is ready');
                        }

                    )
                }).catch(function (err) {
                    viewLogger.log('Error loading screens content..');
                    console.warn('Error loading screens ads content', err);
                });
        
            }
        );
    }

    // Test network
    testNetwork(
        // Success step
        function (counter) {
            viewLogger.log('Online in '+counter);
        },
        // Failed step
        function (counter) {
            viewLogger.log('Failed network tests '+counter);
        },
        // We are online
        function () {

            // Validate host id
            if (!hostScreenId) {
                startHostScreenSetup();
                return;
            }

            // Verify host id 
            resources.sendSignal().then(function () {
                viewLogger.log('Start serving content');
                adsManager.startServingContent().then(function (contentDomain) {
                    console.log('Content is being served at '+contentDomain);
                    initApp(contentDomain);
                }).catch(function (err) {
                    viewLogger.log('Error serving content');
                });
            }).catch(function (resp) {
                if (resp.status != 400 && resp.status != 404) {
                    viewLogger.log('Unknown server response while testing host screen id');
                    return;
                }
                viewLogger.log('Invalid host screen id');
                setTimeout(function () {
                    var devEnv = process.env.DEV || false;
                    var hostScreenIdFilePath
                    if (devEnv) {
                        hostScreenIdFilePath = path.join(process.env.PWD, 'hostscreenid-dev');
                    } else {
                        hostScreenIdFilePath = path.join(process.env.PWD, 'hostscreenid');
                    }
                    fs.unlinkSync(hostScreenIdFilePath);
                    window.location.reload();
                }, 1000);
            })

        }
    )

}]);