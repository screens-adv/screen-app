var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('viewLogger', function () {
    var viewLogger = {};
    
    viewLogger.log = function (log) {
        document.getElementById('current-log').innerHTML = log;
    }
    
    return viewLogger;
});