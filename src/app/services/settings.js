var fs = require('fs-extra');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('settings', function () {
    var devEnv = process.env.DEV || false;
    var screensDataPath = path.join(process.env.TMPDIR || process.env.PWD, 'screensdata');
    var settingsFilePath
    if (devEnv) {
        settingsFilePath = path.join(process.env.PWD, 'settings.dev.json');
    } else {
        settingsFilePath = path.join(process.env.PWD, 'settings.json');
    }
    var settings = fs.readFileSync(settingsFilePath, 'utf-8');
    settings = JSON.parse(settings);
    settings.screensDataPath = screensDataPath;
    settings.adsDataFile = path.join(screensDataPath, 'adsdata.json');
    settings.adsContentDir = path.join(screensDataPath, 'adscontent');
    settings.screensAdsContentDir = path.join(screensDataPath, 'screensadscontent');
    try { fs.mkdirSync(screensDataPath); } catch (err) {}
    try { fs.mkdirSync(settings.adsContentDir); } catch (err) {}
    try { fs.mkdirSync(settings.screensAdsContentDir); } catch (err) {}
    return settings;
});
