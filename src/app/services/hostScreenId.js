var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('hostScreenId', function () {
    var devEnv = process.env.DEV || false;
    var hostScreenIdFilePath
    if (devEnv) {
        hostScreenIdFilePath = path.join(process.env.PWD, 'hostscreenid-dev');
    } else {
        hostScreenIdFilePath = path.join(process.env.PWD, 'hostscreenid');
    }
    try {
        var hostScreenId = fs.readFileSync(hostScreenIdFilePath, 'utf-8');
        return hostScreenId;
    } catch (err) {
        return '';
    }
});