var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('busyFlag', function () {
    var busy = false;
    var busyFlag = {};
    busyFlag.markAsBusy = function () {
        busy = true;
    }
    busyFlag.clear = function () {
        busy = false;
    }
    busyFlag.isBusy = function () {
        return busy;
    }
    return busyFlag
});