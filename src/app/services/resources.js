var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('resources', function ($http, settings, hostScreenId) {
    var resources = {};
    var domain = settings.domain;

    resources.test = function () {
        var deferred = Q.defer();
        var url = domain+'/api/test';
        $http.get(url).then(function (resp) {
            deferred.resolve(resp.data);
        }).catch(deferred.reject);
        return deferred.promise;
    }
    resources.loadHostAdsInstancesByDate = function (dateNumber) {
        var deferred = Q.defer();
        var url = domain+'/api/hostscreen/'+hostScreenId+'/ads/approved?date='+dateNumber;
        $http.get(url).then(function (resp) {
            deferred.resolve(resp.data);
        }).catch(deferred.reject);
        return deferred.promise;
    }
    resources.loadScreensAdsContent = function () {
        var deferred = Q.defer();
        var url = domain+'/api/screens/adscontent';
        $http.get(url).then(function (resp) {
            deferred.resolve(resp.data);
        }).catch(deferred.reject);
        return deferred.promise;
    }
    resources.getHostScreenRequestCode = function () {
        var deferred = Q.defer();
        var url = domain+'/api/hostscreen/request';
        $http.get(url).then(function (resp) {
            deferred.resolve(resp.data);
        }).catch(deferred.reject);
        return deferred.promise;
    }
    resources.verifyHostScreenRequest = function (code) {
        var deferred = Q.defer();
        var url = domain+'/api/hostscreen/request/verify?code='+code;
        $http.get(url).then(function (resp) {
            deferred.resolve(resp.data);
        }).catch(deferred.reject);
        return deferred.promise;
    }
    resources.sendSignal = function () {
        var deferred = Q.defer();
        var url = domain+'/api/hostscreen/'+hostScreenId+'/signal';
        $http.post(url).then(function (resp) {
            deferred.resolve(resp.data);
        }).catch(deferred.reject);
        return deferred.promise;
    }
    resources.loadHostAdsInstancesFromTodayAndTomorrow = function () {
        var deferred = Q.defer();
        var today = new Date();
        var tomorrow = new Date();
        today.setHours(11, 30);
        tomorrow.setHours(11, 30);
        resources.loadHostAdsInstancesByDate(
            today.getTime()
        ).then(function (todayHostAdsInstances) {
            resources.loadHostAdsInstancesByDate(
                tomorrow.getTime()
            ).then(function (tomorrowHostAdsInstances) {
                deferred.resolve({
                    today: todayHostAdsInstances,
                    tomorrow: tomorrowHostAdsInstances
                });
            }).catch(deferred.reject);
        }).catch(deferred.reject);
        return deferred.promise;
    }

    return resources;
});