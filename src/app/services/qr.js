var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('qr', function (settings) {
    var qr = {};
    var qrContainer = document.getElementById('qr-container');
    var qrcode = new QRCode(qrContainer, {
        width: 180,
        height: 180
    });
    qr.generate = function (link) {
        qrcode.clear();
        qrcode.makeCode(link);
    }
    qr.generateRedirectLink = function (adInstanceId) {
        qrContainer.style.opacity = '1';
        var lk = settings.domain+'/api/qr?i='+adInstanceId;
        qr.generate(lk);
    }
    qr.hide = function () {
        qrContainer.style.opacity = '0';
    }
    return qr;
});