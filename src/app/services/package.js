var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('package', function () {
    var packageFilePath = path.join(process.env.PWD, 'package.json');
    var package = fs.readFileSync(packageFilePath, 'utf-8');
    return JSON.parse(package);
});