var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('actions', function (message, busyFlag, codeUpdates, settings) {
    var actions = {};
    actions.performAction = function (action) {
        if (!action || typeof action != 'object') return;
        console.info('Performing action:', action.id);
        switch (action.id) {

            case 'showMessage':
                // Reset timeout
                if (window.showMessageActionTimeout) {
                    clearTimeout(window.showMessageActionTimeout);
                }
                // Start showing message
                message.show({
                    text: action.params.message
                });
                window.showMessageActionTimeout = setTimeout(function () {
                    message.hide();
                }, parseInt(action.params.timeout || '1000'));
            break;

            case 'resetAdsData':
                fs.unlinkSync(settings.adsDataFile);
                window.location.reload();
            break;

            case 'updateCode':
                var f = function () {
                    // Is busy
                    if (busyFlag.isBusy()) {
                        setTimeout(function () {
                            f();
                        }, 1000);
                        return;
                    }
                    // Perform update
                    codeUpdates.updateCode().then(function (codeWasUpdated) {
                        message.show({
                            text: 'Updating code..'
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    })
                }
                f();
            break;

        }
    }
    return actions;
});