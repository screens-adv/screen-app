var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('signal', function (resources, settings, actions) {
    var signal = {};
    signal.send = function () {
        resources.sendSignal().then(function (hostScreenData) {
            // Verify if theres some action to perform
            if (hostScreenData.action) {
                actions.performAction(hostScreenData.action);
            }
        })
    }
    signal.startSending = function () {
        setInterval(function () {
            signal.send();
        }, settings.signalInterval);
    }
    return signal;
});