var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('testNetwork', function (settings, resources) {
    return function (onStep, onStepFail, onSuccess) {
        var counter = 3;
        var missed = 0;
        var f = function () {
            if (!counter) {
                onSuccess();
                return;
            }
            resources.test().then(function () {
                onStep(counter);
                counter--;
                setTimeout(function () {
                    f();
                }, 1000);
            }).catch(function (err) {
                missed++;
                onStepFail(missed);
                setTimeout(function () {
                    f();
                }, 1000);
            });
        }
        f();
    }
});