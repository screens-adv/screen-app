var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('hostScreenRequest', function (resources) {
    var hsRequest = {};
    var devEnv = process.env.DEV || false;
    hsRequest.getRequestCode = function () {
        var deferred = Q.defer();
        var code = window.sessionStorage.getItem('hostScreenRequestCode');
        if (!code) {
            resources.getHostScreenRequestCode().then(function (hostScreenRequest) {
                var code = window.sessionStorage.setItem('hostScreenRequestCode', hostScreenRequest.code+'');
                deferred.resolve(hostScreenRequest.code);
            }).catch(deferred.reject);
            return deferred.promise;
        }
        deferred.resolve(code);
        return deferred.promise;
    }
    hsRequest.startVerifyingRequest = function (code, onAttempt, onVerify) {
        var f = function (onVerify) {
            resources.verifyHostScreenRequest(code).then(function (request) {
                onAttempt(request);
                if (request.hostScreenId) {
                    onVerify(request.hostScreenId);
                    return;
                }
                setTimeout(function () {
                    f(onVerify);
                }, 1000);
            }).catch(function (err) {
                onAttempt(err);
                setTimeout(function () {
                    f(onVerify);
                }, 1000);
            });
        }
        f(onVerify);
    }
    hsRequest.saveHostScreenId = function (hostScreenId) {
        var hostScreenIdFilePath;
        if (devEnv) {
            hostScreenIdFilePath = path.join(process.env.PWD, 'hostscreenid-dev');
        } else {
            hostScreenIdFilePath = path.join(process.env.PWD, 'hostscreenid');
        }
        fs.writeFileSync(hostScreenIdFilePath, hostScreenId);
    }
    return hsRequest;
});