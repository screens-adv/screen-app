var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('codeUpdates', function (settings, package, busyFlag) {
    var codeUpdates = {};
    codeUpdates.updateCode = function () {
        var deferred = Q.defer();
        var indexLock = path.join(process.env.PWD, '.git/index.lock');
        try { fs.unlinkSync(indexLock); } catch (err) {}
        git.pull('origin', 'master', function (err) {
            if (err) {
                deferred.reject(err);
                return;
            }
            var currentVersionFilePath = path.join(process.env.PWD, 'screensdata/currentversion.ignore');
            fs.readFile(currentVersionFilePath, 'utf-8', function (currentVersion) {
                if (package.version != currentVersion) {
                    fs.writeFileSync(currentVersionFilePath, package.version);
                    shell.exec('npm install');
                    deferred.resolve(true);
                    return;
                }
                deferred.resolve(false);
            });
        });
        return deferred.promise;
    }
    codeUpdates.updateAndReload = function () {
        var deferred = Q.defer();
        codeUpdates.updateCode().then(function (codeWasUpdated) {
            if (codeWasUpdated) {
                shell.exec('pm2 save');
                window.location.reload();
                deferred.resolve(true);
                return;
            }
            deferred.resolve(false);
        }).catch(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
    codeUpdates.contiueWithUpdate = function () {
        if (busyFlag.isBusy()) {
            return false;
        }
        return true;
    }
    codeUpdates.startLookingForUpdates = function () {
        var deferred = Q.defer();
        codeUpdates.updateAndReload().then(function (updated) {
            deferred.resolve(updated);
        }).catch(function (err) {
            deferred.reject(err);
        })
        setInterval(function () {
            if (!codeUpdates.contiueWithUpdate()) return;
            codeUpdates.updateAndReload();
        }, settings.codeUpdatesInterval);
        return deferred.promise;
    }
    return codeUpdates;
});