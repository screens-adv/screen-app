var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('message', function () {
    var message = {};
    var messageElement = document.getElementById('message-container');
    /**
     * params.test: string
     */
    message.show = function (params) {
        // if (typeof params != 'object') return;
        messageElement.style.opacity = '1';
        messageElement.innerHTML = params.text;
    }
    message.hide = function () {
        messageElement.style.opacity = '0';
    }
    return message;
});