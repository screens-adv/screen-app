var fs = require('fs-extra');
var path = require('path');
var express = require('express');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');
var download = require('download-file');
var openPort = require('openport');

app.factory('adsManager',
    [ 'settings', 'resources', 'busyFlag', 'viewLogger',
    function (settings, resources, busyFlag, viewLogger) {
    var adsManager = {};
    var adsDataFilePath = settings.adsDataFile;

    adsManager.generatePlaylistFromHostAdsInstancesMap = function (
        hostAdInstancesMap
    ) {
        // Calculate total number of repetitions
        var totalRepetitions = 0;
        Object.keys(hostAdInstancesMap).forEach(function (hostAdInstanceId) {
            var hostAdInstance = hostAdInstancesMap[hostAdInstanceId];
            totalRepetitions += hostAdInstance.adInstance.repetitionsPerDay;
        });
        
        // Calculate host ad instance coef and buff list
        var coefsList = [];
        Object.keys(hostAdInstancesMap).forEach(function (hostAdInstanceId) {
            var hostAdInstance = hostAdInstancesMap[hostAdInstanceId];
            hostAdInstance.repCoef = (
                totalRepetitions /
                hostAdInstance.adInstance.repetitionsPerDay
            );
            hostAdInstance.buffList = [];
            for (var i=0; i<hostAdInstance.adInstance.repetitionsPerDay; i++) {
                coefsList.push({
                    hostAdInstanceId: hostAdInstance.id,
                    adContentId: hostAdInstance.adInstance.adContent.id,
                    coef: hostAdInstance.repCoef*(i+1)
                });
            }
        });

        // Sort coef list and remove coef attribute
        var playlist = coefsList.sort(function (a, b) {
            return (a.coef - b.coef);
        }).map(function (item) {
            item.coef = null;
            item.played = false;
            delete item.coef;
            return item;
        });

        return playlist;
    }

    adsManager.updateAdsData = function () {
        var deferred = Q.defer();
        var today = new Date();
        today.setHours(11, 30);
        resources.loadScreensAdsContent().then(function (screensAdsContent) {
            resources.loadHostAdsInstancesFromTodayAndTomorrow().then(function (
                hostAdsInstances
            ) {
                var adsData = {};
                adsData.lastUpdate = today.getDate() +
                    '-' + today.getMonth() +
                    '-' + today.getFullYear();
                adsData.adsContentMap = {};
                adsData.screensAdsContentMap = {};
                adsData.hostAdsInstancesMaps = {};
                adsData.hostAdsInstancesMaps.today = {};
                adsData.hostAdsInstancesMaps.tomorrow = {};
                screensAdsContent.forEach(function (screensAdsContent) {
                    adsData.screensAdsContentMap[screensAdsContent.id] = screensAdsContent;
                });
                hostAdsInstances.today.forEach(function (
                    todayHostAdInstance
                ) {
                    var adContent = todayHostAdInstance.adInstance.adContent;
                    adsData.adsContentMap[adContent.id] = adContent;
                    adsData.hostAdsInstancesMaps.today[
                        todayHostAdInstance.id
                    ] = todayHostAdInstance;
                });
                hostAdsInstances.tomorrow.forEach(function (
                    tomorrowHostAdInstance
                ) {
                    var adContent = tomorrowHostAdInstance.adInstance.adContent;
                    adsData.adsContentMap[adContent.id] = adContent;
                    adsData.hostAdsInstancesMaps.tomorrow[
                        tomorrowHostAdInstance.id
                    ] = tomorrowHostAdInstance;
                });
                adsData.todayPlaylist = adsManager
                .generatePlaylistFromHostAdsInstancesMap(
                    adsData.hostAdsInstancesMaps.today
                );
                fs.writeFileSync(adsDataFilePath, JSON.stringify(adsData));
                deferred.resolve(adsData);
            }).catch(function (err) {
                deferred.reject(err);
            });
        }).catch(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    adsManager.ensureAdsDataExists = function () {
        var deferred = Q.defer();
        var today = new Date();
        today.setHours(11, 30);
        fs.readFile(adsDataFilePath, 'utf-8', function (err, adsData) {
            // File doesn't exist yet
            if (err) {
                adsManager.updateAdsData().then(function (adsData) {
                    deferred.resolve(adsData);
                }).catch(deferred.reject);
                return;
            }
            // File already exists
            try {
                deferred.resolve(JSON.parse(adsData));
            } catch (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise;
    }

    adsManager.ensureAdsDataIsUpToDate = function () {
        var deferred = Q.defer();
        var today = new Date();
        today.setHours(11, 30);
        var todayDate = today.getDate() +
            '-' + today.getMonth() +
            '-' + today.getFullYear();
        adsManager.ensureAdsDataExists().then(function (adsData) {
            if (adsData.lastUpdate != todayDate) {
                adsManager.updateAdsData().then(function (adsData) {
                    deferred.resolve(true);
                }).catch(deferred.reject);
                return;
            }
            deferred.resolve(false);
        }).catch(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    adsManager.startWatchingForAdsDataUpdates = function (
        onIsUpToDate, onUpdate
    ) {
        adsManager.ensureAdsDataIsUpToDate().then(function (adsDataUpdated) {
            if (adsDataUpdated) {
                onUpdate();
            } else {
                onIsUpToDate();
            }
        });
        setInterval(function () {
            if (busyFlag.isBusy()) return;
            adsManager.ensureAdsDataIsUpToDate().then(function (adsDataUpdated) {
                if (adsDataUpdated) {
                    onUpdate();
                }
            });
        }, settings.adsDataUpdatesInterval);
    }

    adsManager.getAdsData = function () {
        var deferred = Q.defer();
        fs.readFile(adsDataFilePath, 'utf-8', function (err, adsData) {
            if (err) {
                deferred.reject(err);
                return;
            }
            try {
                deferred.resolve(JSON.parse(adsData));
            } catch (err) {
                deferred.reject(err);
            }
        });
        return deferred.promise;
    }

    adsManager.markItemAsPlayedOnCurrentPlaylist = function (itemIndex) {
        var deferred = Q.defer();
        adsManager.getAdsData().then(function (adsData) {
            adsData.todayPlaylist[itemIndex].played = true;
            fs.writeFileSync(
                adsDataFilePath,
                JSON.stringify(adsData)
            );
            deferred.resolve();
        });
        return deferred.promise;
    }

    adsManager.ensureAdsContentIsAvailable = function (
        onTodayContentReady, onTomorrowContentReady
    ) {
        var deferred = Q.defer();
        // Map available content
        var availableAdsContent = {};
        var adsContentDir = settings.adsContentDir;
        fs.readdirSync(adsContentDir).filter(function (entry) {
            return (entry.indexOf(settings.adsContentPrefix)>=0);
        }).forEach(function (entry) {
            entry = entry.replace(settings.adsContentPrefix, '');
            availableAdsContent[entry] = true;
        });
        // Read ads data
        adsManager.getAdsData().then(function (adsData) {
            // List missing content
            var missingContentMap = {};
            ['today', 'tomorrow'].forEach(function (day, priority) {
                Object.keys(adsData.hostAdsInstancesMaps[day])
                .forEach(function (hostAdInstanceId) {
                    var currentDayMap = adsData.hostAdsInstancesMaps[day];
                    var hostAdInstance = currentDayMap[hostAdInstanceId];
                    var adContent = hostAdInstance.adInstance.adContent;
                    // Ad content is not available
                    if (
                        !availableAdsContent[adContent.id] &&
                        !missingContentMap[adContent.id]
                    ) {
                        missingContentMap[adContent.id] = {
                            id: adContent.id,
                            url: adContent.url,
                            priority: priority
                        }
                    }
                })
            });
            // Convert missing map to list
            var missingContentList = Object.keys(
                missingContentMap
            ).map(function (adContentId) {
                return missingContentMap[adContentId];
            }).sort(function (a, b) {
                return a.priority - b.priority;
            });
            // Download missing content
            var stillMissingTodayContent = true;
            var onTodayContentReadyTriggered = false;
            busyFlag.markAsBusy();
            var f = function (cb) {
                if (!missingContentList.length) {
                    cb();
                    return;
                }
                console.log('Pending to download: '+missingContentList.length);
                viewLogger.log(
                    'Pending files to load '+missingContentList.length
                );
                var currentContent = missingContentList.shift();
                if (currentContent.priority==1 && stillMissingTodayContent) {
                    stillMissingTodayContent = false;
                    onTodayContentReadyTriggered = true;
                    onTodayContentReady();
                }
                console.log('Downloading content: '+currentContent.url);
                download(currentContent.url, {
                    directory: adsContentDir,
                    filename: settings.adsContentPrefix+currentContent.id
                }, function (err) {
                    if (!err) {
                        console.info('Downloaded content: '+currentContent.url);
                    } else {
                        console.warn('Error downloading: '+currentContent.url);
                    }
                    f(cb);
                });
            }
            f(function () {
                if (!onTodayContentReadyTriggered) {
                    onTodayContentReady();
                }
                onTomorrowContentReady();
                busyFlag.clear();
                deferred.resolve();
            });
        });
        return deferred.promise;
    }

    adsManager.ensureScreensAdsContentIsAvailable = function () {
        var deferred = Q.defer();
        adsManager.getAdsData().then(function (adsData) {
            var screensAdsContentMap = adsData.screensAdsContentMap;
            var screensAdsContentDir = settings.screensAdsContentDir;
            var availableScreensAdsContent = fs.readdirSync(screensAdsContentDir);
            var availableScreensAdsContentMap = {};
            // Generate available screens ads content map
            availableScreensAdsContent.forEach(function (entry) {
                var avContentId = entry.replace(settings.adsContentPrefix, '');
                availableScreensAdsContentMap[avContentId] = true;
            });
            // Remove unused content
            availableScreensAdsContent.forEach(function (entry) {
                var avContentId = entry.replace(settings.adsContentPrefix, '');
                if (!screensAdsContentMap[avContentId]) {
                    var fullEntryPath = path.join(screensAdsContentDir, entry);
                    try { fs.unlinkSync(fullEntryPath) } catch (err) {}
                }
            });
            // Generate missing content list
            var missingContentList = Object.keys(
                screensAdsContentMap
            ).filter(function (contentId) {
                return !availableScreensAdsContentMap[contentId];
            });
            // Download missing content
            busyFlag.markAsBusy();
            var f = function (onFinish) {
                if (!missingContentList.length) {
                    onFinish();
                    return;
                }
                var contentId = missingContentList.shift();
                var currentContent = screensAdsContentMap[contentId];
                console.log('Downloading content: '+currentContent.url);
                download(currentContent.url, {
                    directory: screensAdsContentDir,
                    filename: settings.adsContentPrefix+currentContent.id
                }, function (err) {
                    if (!err) {
                        console.info('Downloaded screens content: '+currentContent.url);
                    } else {
                        console.warn('Error downloading screens content: '+currentContent.url);
                    }
                    f(onFinish);
                });
            }
            f(function () {
                busyFlag.clear();
                deferred.resolve();
            });
        }).catch(deferred.reject);
        return deferred.promise;
    }

    adsManager.removeUnusedContent = function () {
        var deferred = Q.defer();
        // Current content
        var adsContentDir = settings.adsContentDir;
        var entries = fs.readdirSync(adsContentDir).filter(function (entry) {
            return (entry.indexOf(settings.adsContentPrefix)>=0);
        });
        // Map current ads content
        adsManager.getAdsData().then(function (adsData) {
            var currentAdsContentMap = {};
            ['today', 'tomorrow'].forEach(function (day) {
                Object.keys(adsData.hostAdsInstancesMaps[day])
                .forEach(function (hostAdInstanceId) {
                    var currentDayMap = adsData.hostAdsInstancesMaps[day];
                    var hostAdInstance = currentDayMap[hostAdInstanceId];
                    var adContent = hostAdInstance.adInstance.adContent;
                    currentAdsContentMap[
                        settings.adsContentPrefix+adContent.id
                    ] = true;
                })
            });
            // Filter unused files
            entries.filter(function (entry) {
                return !currentAdsContentMap[entry];
            }).forEach(function (entry) {
                var contentFilePath = path.join(adsContentDir, entry);
                fs.unlink(contentFilePath);
            });
            deferred.resolve();
        });
        return deferred.promise;
    }

    adsManager.startServingContent = function () {
        var deferred = Q.defer();
        openPort.find(function (err, port) {
            if (err) {
                deferred.reject(err);
                return;
            }
            var app = express();
            app.use(express.static(settings.screensDataPath));
            app.listen(port, function () {
                deferred.resolve('http://localhost:'+port);
            });
        });
        return deferred.promise;
    }

    return adsManager;
}]);