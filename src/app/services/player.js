var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var Q = require('q');
var git = require('gulp-git');

app.factory('player', function (adsManager, settings, viewLogger, qr) {
    var player = {};
    player.screensAdsWaitCounterMax = 3;
    player.getAdToPlay = function (contentDomain) {
        var deferred = Q.defer();
        adsManager.getAdsData().then(function (adsData) {
            // Set max wait counter for screens ads
            player.screensAdsWaitCounterMax = Math.min(
                player.screensAdsWaitCounterMax,
                Object.keys(adsData.hostAdsInstancesMaps.today).length
            );
            player.screensAdsWaitCounter = player.screensAdsWaitCounterMax;
            // ---
            var currentPlaylist = adsData.todayPlaylist;
            var everythingIsAlreadyPlayed = !!!currentPlaylist.filter(function (
                item
            ) {
                return !item.played;
            }).length;
            // Some ads are pending to be played
            if (!everythingIsAlreadyPlayed) {
                var missingToPlay = currentPlaylist.filter(function (item, i) {
                    item.playlistIndex = i;
                    return !item.played;
                })[0];
                var item = {
                    adContent: adsData.adsContentMap[
                        missingToPlay.adContentId
                    ],
                    hostAdInstance: adsData.hostAdsInstancesMaps.today[
                        missingToPlay.hostAdInstanceId
                    ],
                    playlistIndex: missingToPlay.playlistIndex
                }
                item.played = missingToPlay.played;
                item.adContent.localPath = contentDomain + '/adscontent/' + 
                    settings.adsContentPrefix+item.adContent.id;
                deferred.resolve(item);
                return;
            }
            // Every ad has been played
            else {
                // Verify if its turn to show screens ads
                if (!player.screensAdsWaitCounter) {
                    console.log('Screens turn', adsData.screensAdsContentMap);
                    var screensAdsContentIdsList = Object.keys(
                        adsData.screensAdsContentMap
                    );
                    // No screens ads available
                    if (!screensAdsContentIdsList.length) return;
                    // Get a random screen ad
                    var adContentPosition = Math.floor(
                        Math.random()*screensAdsContentIdsList.length
                    );
                    var adContentId = screensAdsContentIdsList[adContentPosition];
                    var adContent = adsData.screensAdsContentMap[adContentId];
                    var item = {
                        adContent: adContent,
                        hostAdInstance: null,
                        playlistIndex: -1
                    }
                    item.adContent.localPath = contentDomain + '/screensadscontent/' + 
                        settings.adsContentPrefix+item.adContent.id;
                    player.screensAdsWaitCounter = player.screensAdsWaitCounterMax;
                    deferred.resolve(item);
                    return;
                } else {
                    player.screensAdsWaitCounter--;
                    player.screensAdsWaitCounter = Math.max(player.screensAdsWaitCounter, 0);
                }
                // ---
                var lastAdItemIndex = player.getItemIndexToPlay();
                var missingToPlay = currentPlaylist[
                    lastAdItemIndex%currentPlaylist.length
                ];
                var item = {
                    adContent: adsData.adsContentMap[
                        missingToPlay.adContentId
                    ],
                    hostAdInstance: adsData.hostAdsInstancesMaps.today[
                        missingToPlay.hostAdInstanceId
                    ],
                    playlistIndex: lastAdItemIndex
                }
                item.played = missingToPlay.played;
                item.adContent.localPath = contentDomain + '/adscontent/' + 
                    settings.adsContentPrefix+item.adContent.id;
                deferred.resolve(item);
                return;
            }
        });
        return deferred.promise;
    }

    player.playItem = function (item, onFinish) {
        if (item.hostAdInstance) {
            qr.generateRedirectLink(item.hostAdInstance.adInstanceId);
        } else {
            qr.hide();
        }
        var adContent = item.adContent;
        var imgPlayerContainer = document.getElementById(
            'img-player-container'
        );
        var videoPlayerContainer = document.getElementById(
            'video-player-container'
        );
        var imgElem = document.getElementById('img-elem');
        var videoElem = document.getElementById('video-elem');
        switch (adContent.type) {

            case 'image':
                imgPlayerContainer.style.opacity = '1';
                videoPlayerContainer.style.opacity = '0';
                imgElem.src = adContent.localPath;
                imgElem.style.position = 'absolute';
                imgElem.onload = function () {
                    var ratio = imgElem.width / imgElem.height;
                    if (imgElem.width > imgElem.height) {
                        imgElem.style.width = '100%';
                        imgElem.style.left = '0px';
                        imgElem.style.top = (
                            window.innerHeight/2 - imgElem.height/2
                        )+'px';
                    } else {
                        imgElem.style.height = '100%';
                        imgElem.style.top = '0px';
                        imgElem.style.left = (
                            window.innerWidth/2 - imgElem.width/2
                        )+'px';
                    }
                    setTimeout(function () {
                        onFinish();
                    }, adContent.duration*1000);
                }
            break;

            case 'video':
                videoPlayerContainer.style.opacity = '1';
                imgPlayerContainer.style.opacity = '0';
                videoElem.style.position = 'absolute';
                videoElem.style.left = '0px';
                videoElem.style.top = '0px';
                videoElem.style.width = '100%';
                videoElem.style.height = '100%';
                videoElem.src = adContent.localPath;
                videoElem.play();
                videoElem.onended = function () {
                    onFinish();
                }
            break;

        }
    }

    player.getItemIndexToPlay = function () {
        var currentItemIndex = window.localStorage.getItem('currentItemIndex');
        currentItemIndex = parseInt(currentItemIndex);
        return currentItemIndex+1;
    }

    player.markItemAsPlayed = function (itemIndex, alreadyPlayed) {
        var deferred = Q.defer();
        // Is screens content
        if (itemIndex == -1) {
            deferred.resolve();
            return deferred.promise;
        }
        if (!alreadyPlayed) {
            window.localStorage.setItem('currentItemIndex', '-1');
            adsManager.markItemAsPlayedOnCurrentPlaylist(
                itemIndex
            ).then(function () {
                deferred.resolve();
            });
        } else {
            window.localStorage.setItem('currentItemIndex', itemIndex+'');
            deferred.resolve();
        }
        return deferred.promise;
    }

    player.start = function (contentDomain) {
        console.log('Starting player');
        var f = function () {
            player.getAdToPlay(contentDomain).then(function (item) {
                player.playItem(item, function () {
                    player.markItemAsPlayed(
                        item.playlistIndex,
                        item.played
                    ).then(function () {
                        f();
                    })
                });
            });
        }
        f();
    }

    return player
});