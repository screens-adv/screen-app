var electron = require('electron');
var path = require('path');
var url = require('url');

// Set dev env
var devEnv = process.env.DEV || false;

// Module to control application life
var app = electron.app;

// Module to create native browser window.
var BrowserWindow = electron.BrowserWindow;

// Create window handler
function createWindow () {
    // Create the browser window.
    win = new BrowserWindow({
        kiosk: devEnv ? false : true,
        alwaysOnTop: devEnv ? false : true,
        frame: false
    });

    // and load the index.html of the app.
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))

    // Open the DevTools.
    if (devEnv) {
        win.webContents.openDevTools()
    }

    // Emitted when the window is closed.
    win.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        win = null
    })
}

app.on('ready', createWindow);
